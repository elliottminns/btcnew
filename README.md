<hr />
<div align="center">
    <img src="images/logo.svg" alt="Logo" width='300px' height='auto'/>
</div>
<hr />

[![Build Status](https://travis-ci.org/btcnew/btcnew-node.svg?branch=master)](https://travis-ci.org/btcnew/btcnew-node)
[![Build status](https://ci.appveyor.com/api/projects/status/q66rbt2ux6apjj03/branch/master?svg=true)](https://ci.appveyor.com/project/argakiig/raiblocks/branch/master)
[![GitHub release (latest by date)](https://img.shields.io/github/v/release/btcnew/btcnew-node)](https://github.com/btcnew/btcnew-node/releases/latest)
[![GitHub tag (latest by date)](https://img.shields.io/github/v/tag/btcnew/btcnew-node?color=darkblue&label=beta)](https://github.com/btcnew/btcnew-node/tags)
[![Discord](https://img.shields.io/badge/discord-join%20chat-orange.svg)](https://chat.btcnew.org)

---

### What is Bitcoin New?

Bitcoin New is a digital payment protocol designed to be accessible and lightweight, with a focus on removing inefficiencies present in other cryptocurrencies. With ultrafast transactions and zero fees on a secure, green and decentralized network, this makes Bitcoin New ideal for everyday transactions.

---

### Guides & Documentation

* [Whitepaper](https://btcnew.org/en/whitepaper)
* [Running a Node](https://docs.btcnew.org/running-a-node/overview/)
* [Integration Guides](https://docs.btcnew.org/integration-guides/the-basics/)
* [Command Line Interface](https://docs.btcnew.org/commands/command-line-interface/)
* [RPC Protocol](https://docs.btcnew.org/commands/rpc-protocol/)

Other documentation details can be found at https://docs.btcnew.org.

---

### Links & Resources

* [Bitcoin New Website](https://btcnew.org)
* [Documentation](https://docs.btcnew.org)
* [Discord Chat](https://chat.btcnew.org/)
* [Reddit](https://reddit.com/r/btcnew)
* [Medium](https://medium.com/btcnew)
* [Twitter](https://twitter.com/btcnew)

---

### Want to Contribute?

Please see the [contributors guide](https://docs.btcnew.org/protocol-design/overview/#contributing-code-to-the-btcnew-node).

---

### Contact us

We want to hear about any trouble, success, delight, or pain you experience when
using Bitcoin New. Let us know by [filing an issue](https://github.com/btcnew/btcnew-node/issues), joining us on [Reddit](https://reddit.com/r/btcnew), or joining us on [Discord](https://chat.btcnew.org/).
